## Git Commands
| Befehl | Beschreibung |
|---------|-------------|
| `git init` | Initialisiert ein neues Git-Repository. |
| `git clone` |Lädt eine Kopie eines vorhandenen Git-Repositorys herunter. |
| `git status` | Zeigt den Status des Repositorys an, einschließlich Änderungen an nachverfolgten Dateien. |
| `git add` | Fügt Dateien zum Staging-Bereich hinzu. |
| `git commit` | Speichert einen Snapshot der Änderungen im Repository. |
| `git push` | Sendet lokale Commits an das Remote-Repository. |
| `git pull` | Lädt Updates aus dem Remote-Repository herunter und führt sie mit dem lokalen Repository zusammen. |
