
Befehl| Beispiel| Oft verwendet| Erklärung
-------------|---------------|---------------|-----------
`ipconfig`| ipconfig/all| /? /all| ipconfig ermöglicht es auf Windows-Rechnern, Informationen über die IP-Konfiguration anzuzeigen
`arp`| arp -d "IP-Adresse"| -a, -v| Die ARP (Address Resolution Protocol) Befehle sind eine Möglichkeit um die IP Adresse des Gerätes zu ändern.
`getmac`| getmac| /v, /?| Dieser Befehl ist besonders nützlich, wenn Sie die MAC-Adresse in eine Netzwerkanalyse eingeben möchten oder wenn Sie wissen müssen, welche Protokolle derzeit auf jedem Netzwerkadapter auf einem Computer verwendet werden.
`nslookup`| nslookup root| lserver, ls| nslookup ist ein einfaches, aber sehr praktisches Kommandozeilentool, das in erster Linie dazu verwendet wird, die IP-Adresse zu einem bestimmten Hostname oder den Domain-Namen zu einer bestimmten IP-Adresse zu finden
`ping`| ping "IP-Adresse"| -4, -6| Der Befehl ping sendet ein Datagramm pro Sekunde und gibt für jede empfangene Antwort eine Ausgabezeile aus.
`telnet`| telnet goole.ch 80| set, un| Benutzer können mit diesem Tool einen Ping an einen Port senden und herausfinden, ob dieser offen ist.
`netstat`| netstat| -i , -h| Der Befehl netstat erzeugt eine Anzeige, in der Netzwerkstatus und Protokollstatistiken aufgeführt werden. Sie können den Status von TCP-, SCTP- und UDP-Endpunkten in einem Tabellenformat anzeigen.
`route`| route print| -net, -host| Mit dem Route Command kannst du deine Network Routing Tables anschauen oder manuell editieren
`tracert`| tracert www.youtube.com| -j, -d| Traceroute ist ein Kommandozeilen-Tool, das per CMD-Befehl gestartet werden kann und dem Benutzer Informationen über den Weg von Datenpaketen im Netzwerk liefert.
`pathping`| pathping youtube.com| /g "hostlist", /6, /4| Die Funktionsweise von Pathping ist wie beim Tracert Befehl. Mit dem Pathping Befehl werden mehrere ICMP-Datenpakete an den Zielrechner geschickt.
