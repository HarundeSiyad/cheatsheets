# Navigation für Linux


Befehl| Beispiel| Erklärung|  Für was kann man es brauchen|
-------------|---------------|---------------|-----------
`help`| help(tab)| Zeigt die Hilfe von den bash Befehlen.| Wenn man nicht weiss, was ein bestimmter Kommand macht.
`cd`| cd Desktop| Wechselt das aktive Verzeichnis.| Wenn man eine Datei erstellen oder Löschen will mit mkdir/rmdir.
`mkdir`|  mkdir Hallo|  Erstellt ein Verzeichnis.| Wenn man einen neuen Ordner erstellen will.
`pwd`|  pwd|  Zeigt momentanes Verzeichnis.| Wenn man wissen will, ob man im richtigen Ordner ist.
`rmdir`|  rmdir Hallo|  Ermöglicht es ein Verzeichnis zu löschen.| Wenn man einen Ordner löschen möchte.
`sudo`| sudo apt-get install |Er ermöglicht es Befehle auszuführen mit Rechten eines anderen Benutzers. | Wenn man etwas installieren will aber nicht Admin Rechte hat.
`touch`| touch Documents/text.txt | leere Datei erstellen| Wenn man eine leere text Datei braucht.
`mv`| mv documents.txt Documents/| Man kann seine Dateien verschieben.| Wenn man sein Verzeichnis neu strukturieren muss.
`cp`| cp Documents/document Pictures/| Man kann Verzeichnisse kopieren und an einem anderem Ort einfügen.| Wenn man sein Verzeichnis neu strukturieren muss.
`ls`| ls Documents/| Es schreibt den inhalt des verzsichnisses| Wenn man wissen will was darin steht.
`vim`| vim "file.txt"| Man kann damit text editieren| Wenn man schnell noch nach Fehlern schauen möchte..
