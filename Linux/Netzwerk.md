## Netzwerk Commands

| Befehl | Beschreibung |
|---------|-------------|
| `ifconfig` | Zeigt die Konfiguration der Netzwerkschnittstellen. |
| `ip addr` | Zeigt die Konfiguration von IPv4- und IPv6-Adressen auf Netzwerkschnittstellen. |
| `ip route` | Zeigt die IP-Routing-Tabelle. |
| `ping` | Testet die Erreichbarkeit eines entfernten Hosts. |
| `traceroute` | Zeigt den Pfad an, den ein Paket zu einem entfernten Host nimmt. |
| `telnet` | Öffnet eine Befehlszeilenverbindung zu einem Remotehost. |
| `ssh` | Öffnet eine Secure-Shell-Verbindung zu einem Remote-Host. |
